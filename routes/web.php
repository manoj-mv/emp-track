<?php

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Mail\NotifyUserAdded;
use App\Models\Employee;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login',[LoginController::class,'authenticate'])->name('login');
Route::get('logout',[LoginController::class,'logout'])->name('logout');

Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/auditor-dashboard', function () {
    return view('auditor');
})->name('auditor');

// Route::get('/add-user', function () {
//     return view('add-user');
// })->name('add_user');

Route::get('/add-user',[UserController::class,'show_form'])->name('add_user');

Route::get('/insert-user',[UserController::class,'insert_user'])->name('insert_user');
Route::get('/update-user/{id}',[UserController::class,'update_user'])->name('update_user');
Route::post('/delete-user/{id}',[UserController::class,'delete_user'])->name('delete_user');
Route::get('/user-status/{id}',[UserController::class,'delete_user'])->name('delete_user');
Route::get('getAllAdmins',[UserController::class,'getAllAdminUsers'])->name('getAllAdmin');

Route::get('edit/{edit_id}',[UserController::class,'getUserData']);

Route::get('/notify-user-added',function(Request $request){
    try{
        Mail::to($request->user())->send(new NotifyUserAdded($request->email,$request->password,$request->role));
    }catch(\Swift_TransportException $transportExp){
        // return $transportExp->getMessage();
        return back()->withErrors([
            'error' => 'Error occured in sending email.',
        ]);
    }
    
    return redirect()->route('add_user')->with('success','User data inserted.Notification mail sent to user.');
})->name('notify-user-added');




Route::get('forget-password',function(){
    return view('forget_pwd');
})->name('forgetPwd');

Route::get('admin-list',function(){
    return view('admin-list');
});




Route::get('add-employee',[EmployeeController::class,'create'])->name('add-employee');

Route::post('insert-employee',[EmployeeController::class,'store'])->name('insert-employee');

Route::get('employee-list',function(){
    return view('employee-list');
});
Route::get('getAllEmployees',[EmployeeController::class,'index'])->name('getAllEmployees');
Route::get('/delete-employee/{id}',[EmployeeController::class,'destroy'])->name('delete_employee');
Route::get('edit-employee/{id}',[EmployeeController::class,'edit'])->name('edit-employee');
Route::post('update-employee/{id}',[EmployeeController::class,'update'])->name('update-employee');

Route::get('test',function(){
    $x= Hash::make('test@123');
    return $x;
}); 



