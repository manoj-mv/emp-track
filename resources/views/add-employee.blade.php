@extends('layouts.dashboard')

@section('css')
.form-group{
    padding: 0 1rem;
}

.radio-row{
    display: flex;
}

.radio-container{
    display: flex;
    flex-direction: column;
    /* margin: 2rem; */
    margin-bottom: .5rem;
}

.form-check{
    padding-right: 5rem;;
}

.h-underline{
    color:red;
}
@endsection

@section('content')

<h2>Add Employee</h2>
<hr class="h-underline">

<span class="text-danger">@error('error'){{$message}} @enderror</span> 

@if(Session::has('success'))
<p class="alert alert-info">{{ Session::get('success') }}</p>
@endif
<form action="{{route('insert-employee')}}" method="post" enctype="multipart/form-data" style="min-width:80%;">
     @csrf                   
    <div class="form-row">
        <div class="form-group col-md-7">
            <label for="empId">Employee Id</label>
            <input type="text" class="form-control" name="employee_id" id="empId" placeholder="Employee id" value={{old('employee_id')}}>
            <span class="text-danger">@error('employee_id'){{$message}}@enderror</span> 
        </div>

        <div class="form-group col-md-5">
            <label for="salutation">Saluation</label>
            <select id="salutation" name="salutation" class="form-control">
              <option value="">--select--</option>
              <option value="Mr." {{ old('salutation') == "Mr." ? 'selected' : ''}}>Mr.</option>
              <option value="Mrs." {{ old('salutation') == "Mrs." ? 'selected' : ''}}>Mrs.</option>
              <option value="Ms." {{ old('salutation') == "Ms." ? 'selected' : ''}}>Ms.</option>
            </select>
            <span class="text-danger">@error('salutation'){{$message}}@enderror</span>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="first_name">First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name"  value={{old('first_name')}} >
            <span class="text-danger">@error('first_name'){{$message}}@enderror</span>
        </div>
        <div class="form-group col-md-6">
           <label for="last_name">Last Name</label>
           <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value={{old('last_name')}}>
           <span class="text-danger">@error('last_name'){{$message}}@enderror</span>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="asd@as.in" value={{old('email')}}>
            <span class="text-danger">@error('email'){{$message}}@enderror</span>
        </div>
        <div class="col-md-6 radio-container">
            <div>
                <label >Gender</label>
            </div>
            <div class="radio-row col-md-6">
                    
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="M" {{ (old('gender') == 'M') ? 'checked' : ''}}>
                  <label class="form-check-label" for="inlineRadio1">Male</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="F" {{ (old('gender') == 'F') ? 'checked' : ''}}>
                  <label class="form-check-label" for="inlineRadio2">Female</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="O" {{ (old('gender') == 'O') ? 'checked' : ''}}>
                  <label class="form-check-label" for="inlineRadio3">Others</label>
                </div>
            </div>
            <span class="text-danger">@error('gender'){{$message}}@enderror</span>
        </div>
    </div>
    
    
    <div class="form-row">
        <div class="form-group addr col-md-12">
            <label for="inputAddress">Address</label>
            <textarea name="address" id="" cols="30" rows="3" class="form-control" >{{old('address')}}</textarea>
            <span class="text-danger">@error('address'){{$message}}@enderror</span>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
                <label for="country">Country</label>
                <select id="country" name="country" class="form-control">
                  <option value="">Choose...</option>
                  @foreach ($countries as $item)
                  <option value="{{$item->id}}" {{ old('country') == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                  @endforeach
                  
                </select>
                <span class="text-danger">@error('country'){{$message}}@enderror</span>
        </div>
        <div class="form-group col-md-6">
            <label for="doj">Date of Joining</label>
            <input type="date" class="form-control" name="doj" id="doj" placeholder="Employee id">
            <span class="text-danger">@error('doj'){{$message}}@enderror</span>
        </div>
    </div>

    <div class="form-row">
    </div>    
        
        
    <div class="form-row">
        <div class="form-group col-md-6">
          <label for="city">City</label>
          <input type="text" class="form-control" id="city" name="city" value={{old('city')}}>
          <span class="text-danger">@error('city'){{$message}}@enderror</span>
        </div>
        <div class="form-group col-md-4">
          <label for="state">State</label>
          <select id="state" name="state" class="form-control">
            <option value="" >Choose...</option>
            @foreach ($states as $item)
              <option value="{{$item->id}}" {{ old('state') == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
              @endforeach
          </select>
          <span class="text-danger">@error('state'){{$message}}@enderror</span>
        </div>
        <div class="form-group col-md-2">
          <label for="pin_code">Zip</label>
          <input type="text" class="form-control" name="pincode" id="pin_code" value={{old('pincode')}}>
          <span class="text-danger">@error('pincode'){{$message}}@enderror</span>
        </div>
    </div>
    
    {{-- <div class="form-group">
        <label for="resume">Upload Resume</label>
        <input type="file" name="resume" id="resume" >
        <span class="text-danger">@error('resume'){{$message}}@enderror</span>
    </div> --}}
    <div class="form-group">
        
        <label for="resume">Upload Resume</label>
        <input type="file" name="resume" id="resume" >
        <span class="text-danger">@error('resume'){{$message}}@enderror</span>
    </div>
    
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Add</button>
    </div>    
 </form>
 
@endsection