@extends('layouts.dashboard')

@section('content')
<script>

var minDate, maxDate;
 
 // Custom filtering function which will search data in column four between two values
 $.fn.dataTable.ext.search.push(
     function( settings, data, dataIndex ) {
         var min = minDate.val();
         var max = maxDate.val();
         var date = new Date( data[7] );
  
         if (
             ( min === null && max === null ) ||
             ( min === null && date <= max ) ||
             ( min <= date   && max === null ) ||
             ( min <= date   && date <= max )
         ) {
             return true;
         }
         return false;
     }
 );
 
    $(document).ready( function () {
    
    // get currrent user role stored in session
    var role ='{{session('role')}}';
    var table =$('#table_id').DataTable({
        "ajax": "getAllEmployees",
        "columns": [
            { "data": "id" },
            { "data": "employee_id" },
            { "data": "salutation" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "email" },
            { "data": "gender",},
            { "data": "doj",},
           
            {"mRender": function ( data, type, row ) {
                        return '<a class="btn btn-primary" href=edit-employee/'+row.id+'>Edit</a> ';
            }},
            {"mRender": function ( data, type, row ) {
                        return '<a class="btn btn-danger" href=delete-employee/'+row.id+'>Delete</a> ';
            }},
        ],
        "dom": 'lBfrtip',
        "buttons": [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0,1,2,3,4,5,6,7]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6,7 ]
                }
            },
        ],

     });
      // Create date inputs
      minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    });
     $('#min, #max').on('change', function () {
        table.draw();
    });
   

     $('#table_id tbody').on( 'click', '.toggle_btn', function (event) {
        // console.log(event);
     });

     //  hide columns if user is auditor
     if(role=="Auditor"){
        table.columns([8,9]).visible(false);
     }
    
 } );

 function toggleOps(event,id){
        console.log(event.currentTarget.checked,id);

}
function setCurrentStat(event,isActive,id){
    console.log(isActive);
    if(isActive==1){
        console.log('test');
        console.log('toggle_btn'+id);
        var btnId='toggle_btn'+id;
        console.log(document.getElementById(btnId));
        // document.getElementById(btnId).setAttribute(checked,true);
    }
}
 </script>

<div class="row">
    <div class="col-sm-1">
        <label>Filter by date :</label>
    </div>
    <div class="col-sm-3">
        {{-- <label for="min"> Minimum date:</label> --}}
        <input type="text" class="form-control" id="min" name="min" placeholder="Select min date">
    </div>
    <div class="col-sm-3">
        {{-- <label for="max"> Minimum date:</label> --}}
        <input type="text" id="max" class="form-control" name="max" placeholder="Select max date">
    </div>
</div>
<div class="row">
    @if(Session::has('success'))
        <p class="alert alert-info">{{ Session::get('success') }}</p>
    @endif
    <div class="cols-xs-6">

    {{-- <table border="0" cellspacing="5" cellpadding="5">
            <tbody><tr>
                <td>Minimum date:</td>
                <td><input type="text" id="min" name="min"></td>
            </tr>
            <tr>
                <td>Maximum date:</td>
                <td><input type="text" id="max" name="max"></td>
            </tr>
        </tbody>
    </table> --}}

    <table id="table_id" class="display" style="width:98%">
        <caption><h4>Employee List</h4></caption>
        <thead>
            <tr>
                <th>Id</th>
                <th>Employee Id</th>
                <th>Salutation</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Join Date</th>
                <th>Edit</th>
                <th>Delete</th> 
                {{-- <th>Join Date</th>
                <th>Actions</th> --}}
            </tr>
        </thead>
        <tbody id="tbody">

        </tbody>

        {{-- <tfoot>
            <tr>
                <th>Employee Id</th>
                <th>Salutation</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Join Date</th>
            </tr>
        </tfoot> --}}
    </table>

    </div>
</div>
@endsection