
@extends('layouts.dashboard')

@section('content')
<script>
    $(document).ready( function () {
    //  var table =$('#table_id').DataTable({
    //      ajax:'/getAllAdmins',
    //      "columnDefs": [ 
    //         {                             //https://datatables.net/examples/ajax/null_data_source.html
    //          "targets": -2,                            //
    //          "data": null,
    //          "defaultContent": "<button id='edit'>Edit</button>"
    //         },
    //         {                             
    //          "targets": -1,                           
    //          "data": null,
    //          "defaultContent": "<button id='del'>Delete</button>"
    //         },
    //         {                             
    //          "targets": -3,                           
    //          "data": null,
    //          "defaultContent": "<label class='switch'><input type='checkbox' class='toggle_btn' ><span class='slider round'></span></label> <input type='checkbox' data-toggle='toggle'>"
    //         },
    //         {
    //         label: "Is Active:",
    //         name: "Is Active",
    //         type: "checkBox",
    //         def: true
    //     }
         
    //      ]
    //  });
    //  edit button
    
    // get currrent user role stored in session
    var role ='{{session('role')}}';
    console.log(role);
    var table =$('#table_id').DataTable({
        "ajax": "getAllAdmins",
        "columns": [
            { "data": "id" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "email" },
            { "data": "role",},
            // { "data": "isActive" }
            {"mRender": function ( data, type, row ) {
                        return `<label class='switch'><input type='checkbox' class='toggle_btn' ${setCurrentStat(event,row.isActive,row.id)} 
                        id="toggle_btn${row.id}" onChange='toggleOps(event,${row.id})'><span class='slider round'></span></label>`;
            }},
            {"mRender": function ( data, type, row ) {
                        return '<a class="btn btn-primary" href=edit/'+row.id+'>Edit</a> ';
            }},
            {"mRender": function ( data, type, row ) {
                        // return '<a class="btn btn-danger" href=delete-user/'+row.id+'>Delete</a> ';
                        return `<a class="btn btn-danger"   data-toggle="modal" data-target="#deleteModal" data-id=${row.id}>Delete</a>`;
            }},

            
        ],
        "dom": 'lBfrtip',
        "buttons": [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0,1,2,3,4]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4 ]
                }
            },
        ],
     });
     $('#table_id tbody').on( 'click', '#edit', function () {
         var data = table.row( $(this).parents('tr') ).data();
         // alert( data[0] +"'s salary is: "+ data[ 5 ] );
         console.log(data);
         window.location.href = "edit/"+data[0];
         } );

    // delete button
    $('#table_id tbody').on( 'click', '#del', function () {
         var data = table.row( $(this).parents('tr') ).data();
         window.location.href = "delete-user/"+data[0];
         } );

   

     $('#table_id tbody').on( 'click', '.toggle_btn', function (event) {
        // console.log(event);
     });

    //  hide columns if user is auditor
     if(role=="Auditor"){
        table.columns([5,6,7]).visible(false);
     }
    
 } );


 function confirm_delete(id){
        console.log('test',id);
}

 function toggleOps(event,id){
        console.log(event.currentTarget.checked,id);

}
function setCurrentStat(event,isActive,id){
    // console.log(isActive);
    if(isActive==1){
        // console.log('test');
        // console.log('toggle_btn'+id);
        var btnId='toggle_btn'+id;
        // console.log(document.getElementById(btnId));
        // document.getElementById(btnId).setAttribute(checked,true);
    }
}
 </script>
    <div class="row">
        @if(Session::has('success'))
            <p class="alert alert-info">{{ Session::get('success') }}</p>
        @endif
        <div class="cols-xs-6">
        <table id="table_id" class="display" style="width:98%">
            <caption><h4>Admin Users</h4></caption>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>IsActive</th>
                    <th>Edit</th>
                    <th>Delete</th> 
                    {{-- <th>Join Date</th>
                    <th>Actions</th> --}}
                </tr>
            </thead>
            <tbody id="tbody">

            </tbody>

            {{-- <tfoot>
                <tr>
                    <th>Employee Id</th>
                    <th>Salutation</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Join Date</th>
                </tr>
            </tfoot> --}}
        </table>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Company</h4>
                </div>
                <div class="modal-body">
                        Are you sure you want to delete?
                </div>
                <div class="modal-footer">
                    <form id="userForm" action="" method="post">
                        @csrf
                        {{-- @method('DELETE') --}}
                        <input type="hidden" name="id">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
            var id = button.data('id');
    
        $('#userForm').attr("action", "{{ url('delete-user') }}" + "/" + id);
    });
    </script>
@endsection



