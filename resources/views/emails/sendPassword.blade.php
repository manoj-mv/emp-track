@component('mail::message')
# You were added to emp-track database

Welcome to EMP-Track.<br>
Your role: {{$role}} <br>
email: {{$email}}<br>
password: {{$password}}<br>

@component('mail::button', ['url' => 'http://localhost:8000'])
Login Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
