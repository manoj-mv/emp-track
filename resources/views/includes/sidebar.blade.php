@if (session('role'))
    {{ $role = session('role') }}
@endif
<ul class="nav nav-pills nav-stacked">  
    <li class="{{ Request::path() ==  'dashboard' ? 'active' : ''  }}"><a href="/dashboard">Dashboard</a></li>
    <li class="{{ Request::path() ==  'admin-list' ? 'active' : ''  }}"><a href="/admin-list">View admin users</a></li>
    <li class="{{ Request::path() ==  'add-user' ? 'active' : ''  }}"><a href="/add-user">Add Admin User</a></li>

    <li class="{{ Request::path() ==  'employee-list' ? 'active' : ''  }}"><a href="/employee-list">View Employees</a></li>
    <li class="{{ Request::path() ==  'add-employee' ? 'active' : ''  }}"><a href="/add-employee">Add Employee</a></li>
    
</ul>
<br>