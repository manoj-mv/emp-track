@extends('layouts.app')


@section('content')

<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">fORGOT pASSWORD</div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                <span class="text-danger">@error('error'){{$message}}
                    
                @enderror</span>   
                <form id="loginform" class="form-horizontal" role="form" action="{{route('login')}}" method="post">
                    @csrf   
                    <span class="text-danger">@error('email'){{$message}} @enderror</span>   
                    <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input id="login-email" type="text" class="form-control" name="email"  placeholder="Email" value={{old('email')}}>                                        
                    </div>
                    

                    <div style="margin-top:10px" class="form-group">
                            <!-- Button -->

                            <div class="col-sm-12 controls">
                              {{-- <input id="btn-login" type="submit"  class="btn btn-success" value="Login "> --}}
                              <input type="submit" value="Send Password Reset Link" class="btn btn-block btn-success">
                              {{-- <a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a> --}}

                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12 control">
                                {{-- <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                    Don't have an account! 
                                <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                    Sign Up Here
                                </a>
                                </div> --}}
                            </div>
                        </div>    
                    </form>     



                </div>                     
            </div>  
</div>
@endsection