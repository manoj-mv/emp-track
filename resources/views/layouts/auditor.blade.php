@include('includes.admin-header')

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h3>EMP-Track</h3>
      @include('includes.sidebar-auditor')
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Blog..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div>
    </div>
    <div class="col-sm-7">
    </div> 
    <div class="col-sm-2 py-2">
      <a href="{{ url('/logout') }}" class="btn btn-primary"> logout </a>
    </div>
    <div class="col-sm-9">
      @yield('content')
     
    </div>
  </div>
</div>

@include('includes.admin-footer')
