@extends('layouts.dashboard')

@section('content')

<h3>Add User</h3>
<span class="text-danger">@error('error'){{$message}} @enderror</span> 

@if(Session::has('success'))
<p class="alert alert-info">{{ Session::get('success') }}</p>
@endif

<form class="well form-horizontal" method="get" action="{{route('insert_user')}}">
    <fieldset>
       <div class="form-group">
          <label class="col-md-4 control-label">First Name</label>
          <div class="col-md-8 inputGroupContainer">
             <div class="input-group">
                 <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                 <input id="first_name" name="first_name" placeholder="First Name" class="form-control" required="true" value="" type="text"></div>
          </div>
       </div>
       <div class="form-group">
        <label class="col-md-4 control-label">Last Name</label>
        <div class="col-md-8 inputGroupContainer">
           <div class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
               <input id="last_name" name="last_name" placeholder="Last Name" class="form-control" required="true" value="" type="text"></div>
        </div>
      </div>
       
      <div class="form-group">
         <label class="col-md-4 control-label">Email</label>
         <div class="col-md-8 inputGroupContainer">
            <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="email" name="email" placeholder="Email" class="form-control" required="true" value="" type="text"></div>
         </div>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">Role</label>
        <div class="col-md-8 inputGroupContainer">
           <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon glyphicon-user"></i></span>
            <select id="role" name="role" class="form-control" required="true">
                <option value="">--select--</option>
                @foreach ($results as $item)
                    <option value="{{$item->id}}">{{$item->role}}</option>
                @endforeach
            </select>
           </div>
                
        </div>
     </div>
     <div class="col-md-4">
     </div>
     <div class="col-md-8">
        <input type="submit" value="Add user" class="btn btn-success">
     </div>
    
    </fieldset>
    
 </form>

@endsection
