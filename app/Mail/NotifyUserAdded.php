<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyUserAdded extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $password;
    public $role;
    public $route ='http://localhost:8000';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$password,$role)
    {
        //
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.sendPassword');
    }
}
