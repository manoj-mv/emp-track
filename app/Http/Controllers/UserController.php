<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\str_random;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserController extends Controller
{
    //
    function show_form(){
        $results = DB::select('select * from user_roles');
        return view('add-user',compact('results'));
    }

    /**
     * insert user data in db
     */
    function insert_user(Request $request){
        $random_pwd =Str::random(8);
        $hashed_pwd = Hash::make($random_pwd);
        $timestamp = now();
        $user_id =  Auth::id();
        $result = DB::table('users')->insert(['first_name'=>$request->first_name,
                                    'last_name'=>$request->last_name,
                                    'email'=>$request->email,
                                    'role_id'=>$request->role,
                                    'password'=>$hashed_pwd,
                                    'created_by'=>$user_id,
                                    'created_at'=>$timestamp,
                                    'updated_by'=>$user_id,
                                    'updated_at'=>$timestamp
                                    ]);
        if($result){
            $result = DB::select('select * from user_roles where id = :id',['id'=>$request->role]);
            // return $result[0]->role;
            
            foreach ($result as $row) {
                $role =$row->role;
            }
            return redirect()->route('notify-user-added',
                        ['email'=>$request->email,'password'=>$random_pwd,'role'=>$role]);
        }else{
            return $result;
        }
        
        
    }

    function update_user(Request $request,$id){
        if($request){
            $timestamp = now();
            $user_id =  Auth::id();
            $user =User::find($id);
            $user->first_name= $request->first_name;
            $user->last_name=$request->last_name;
            $user->email=$request->email;
            $user->role_id=$request->role;
            $user->updated_by =$user_id;
            $user->updated_at =$timestamp;
            $status =$user->save();
        }
        
        if($status){
            return redirect()->back()->with('success','User data updated.');
        }
        else{
            return redirect()->back()->withErrors('error','Data updation failed for some reason.');
        }
    }

    function delete_user($id){
        $user = User::find($id);
        $status = $user->delete();
        // return $status;
        if($status){
            return redirect()->back()->with('success','User deleted.');
        }
        else{
            return redirect()->back()->withErrors('error','Deletion failed.');
        }
    }

    function getAllAdminUsers(){
        $results = DB::table('users')
        ->join('user_roles','users.role_id','=','user_roles.id')
            ->select('users.*','user_roles.role')
            ->get();
        $data =array();
        // foreach($results as $row){
        //     // $data[]=$row;
        //     $role= UserRole::where('id',$row->role_id)->get('role');
        //     $data[] =[
        //         $row->id,
        //         $row->first_name,
        //         $row->last_name,
        //         $row->email,
        //         // $role[0]->role,
        //         // $row->isActive
        //         // $row['doj']
        //     ];
        // }  
        
        // $d = array("data"=>$data);
        // echo json_encode($d);
        // echo $d;
        $obj = json_encode(['data'=>$results]);
        echo $obj;
    }

    function getUserData($id){
        $roles = DB::select('select * from user_roles');
        $user_info = User::where('id',$id)
                    ->get();
        $user_info = $user_info[0];
        return view('edit-user',compact('user_info','roles'));
        // return $user_info->first_name;
    }
}
