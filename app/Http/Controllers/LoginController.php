<?php

namespace App\Http\Controllers;

use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {   
        // return $request;
        // var_dump($request);
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required','min:5'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            $role = UserRole::find(Auth::user()->role_id)->role;
            session(['role' => $role]);
            // dd($role);
            return redirect()->intended('dashboard');
        }

        return back()->withErrors([
            'error' => 'The provided credentials do not match our records.',
        ]);
    }


    public function logout(Request $request){
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function sendPasswordResetLink(Request $request){
        return $request;
    }
}
