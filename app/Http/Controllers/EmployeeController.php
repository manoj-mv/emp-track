<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Employee;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $results = DB::table('employee')
            ->leftjoin('states','states.id','=','employee.state')
            ->leftjoin('countries','countries.id','=','employee.country')
            ->select('employee.*','countries.name AS country','states.name AS state')
            ->get();
        // dd($results);
        $data =array();
        $obj = json_encode(['data'=>$results]);
        echo $obj;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries= Country::all();
        $states = State::all();
        return view('add-employee',compact('countries','states'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $form_data =$request->all();
        // dd($request->all(),$form_data['employee_id']);
    #v

    $request->validate([
        'employee_id'=>['required','min:5'],
        'salutation'=>['required'],
        'first_name'=>['required','min:2'],
        'last_name'=>['required'],
        'email' => ['required', 'email'],
        'gender' => ['required'],
        'address'=>['required','min:5'],
        'country'=>['required'],
        'doj'=>['required'],
        'city'=>['required'],
        'state'=>['required'],
        'pincode'=>['required'],
        'resume'=>['required'],
    ]);
    if($request){
        $timestamp = now();
        $user_id =  Auth::id();
        // dd($user_id);
        $employee = new Employee;
        $employee->employee_id = $request->get('employee_id');
        $employee->salutation  = $request->get('salutation');
        $employee->first_name  = $request->get('first_name');
        $employee->last_name  = $request->get('last_name');
        $employee->email  = $request->get('email');
        $employee->gender  = $request->get('gender');
        $employee->address  = $request->get('address');
        $employee->country  = $request->get('country');
        $employee->doj  = $request->get('doj');
        $employee->city  = $request->get('city');
        $employee->state  = $request->get('state');
        $employee->pincode  = $request->get('pincode');
        $employee->updated_at  = $timestamp;
        $employee->updated_by  = $user_id;
        $employee->created_at  = $timestamp;
        $employee->created_by  = $user_id;
        
        $path = Storage::putFile('public/resumes', $request->file('resume'));
        $employee->resume  = $path;
        // dd($path);
        $status =$employee->save();
    }
        
        if($status){
            return redirect()->back()->with('success','Employee data inserted.');
        }
        else{
            return redirect()->back()->withErrors('error','Data insertion failed for some reason.');
        }

        // return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $countries= Country::all();
        $states = State::all();
        $employee_info = Employee::where('id',$id)
                    ->get();
        
        $employee_info = $employee_info[0];
        // dd($employee_info->state);
        // $employee_state = State::find($employee_info->state)->name;
        // return $employee_state;
        return view('edit-employee',compact('countries','states','employee_info'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        if($request){
            $request->validate([
                'employee_id'=>['required','min:5'],
                'salutation'=>['required'],
                'first_name'=>['required','min:2'],
                'last_name'=>['required'],
                'email' => ['required', 'email'],
                'gender' => ['required'],
                'address'=>['required','min:5'],
                'country'=>['required'],
                'doj'=>['required'],
                'city'=>['required'],
                'state'=>['required'],
                'pincode'=>['required'],
            ]);
            $timestamp = now();
            $user_id =  Auth::id();
            // dd($user_id);
            // $employee = new Employee;
            $employee =Employee::find($id);
            $employee->employee_id = $request->get('employee_id');
            $employee->salutation  = $request->get('salutation');
            $employee->first_name  = $request->get('first_name');
            $employee->last_name  = $request->get('last_name');
            $employee->email  = $request->get('email');
            $employee->gender  = $request->get('gender');
            $employee->address  = $request->get('address');
            $employee->country  = $request->get('country');
            $employee->doj  = $request->get('doj');
            $employee->city  = $request->get('city');
            $employee->state  = $request->get('state');
            $employee->pincode  = $request->get('pincode');
            $employee->updated_at  = $timestamp;
            $employee->updated_by  = $user_id;
            $employee->created_at  = $timestamp;
            $employee->created_by  = $user_id;
            // dd($request->hasFile('resume'));
            // dd($employee->resume);
            if($request->hasFile('resume')){
                Storage::delete($employee->resume);
                $path = Storage::putFile('public/resumes', $request->file('resume'));
               
                // dd($path);
                $employee->resume  = $path;
            }
            // dd($path);
            $status =$employee->save();
            if($status){
                return redirect()->back()->with('success','Employee data updated.');
            }
            else{
                return redirect()->back()->withErrors('error','Data updation failed for some reason.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $employee = Employee::find($id);
        $status = $employee->delete();
        // return $status;
        if($status){
            return redirect()->back()->with('success','Employee deleted.');
        }
        else{
            return redirect()->back()->withErrors('error','Deletion failed.');
        }
    }

    function getResume($path){
        return Storage::get($path);
    }
}
