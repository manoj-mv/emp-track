<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnsToEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            //
            $table->string('employee_id')->unique();
            $table->string('salutation');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->char('gender');
            $table->date('doj'); 
            $table->string('address');
            $table->string('city');
            $table->unsignedBigInteger('state');
            $table->unsignedBigInteger('country');
            $table->unsignedBigInteger('pincode');
            $table->string('resume');
            
            $table->unsignedBigInteger('created_by')->after('created_at');
            
            $table->unsignedBigInteger('updated_by')->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            //
        });
    }
}
