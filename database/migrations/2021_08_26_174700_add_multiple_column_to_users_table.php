<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('last_name')->after('first_name');
            $table->unsignedBigInteger('role_id')->after('last_name');
            
            $table->unsignedBigInteger('created_by')->after('remember_token');
            
            $table->unsignedBigInteger('updated_by')->after('created_at');
            
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')
                    ->references('id')
                    ->on('user_roles')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
